﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
[AspNetCompatibilityRequirements(RequirementsMode =
            AspNetCompatibilityRequirementsMode.Allowed)]
public class Service : IService
{

    DataTable table;
    public void DoWork()
    {

    }

    public void AddElection(string elecName, string StartDate, string EndDate)
    {
        StartDate = StartDate.Replace('a', '-');
        EndDate = EndDate.Replace('a', '-');
        table = new DataTable();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("addElection", @"election_name", elecName, @"startD", Convert.ToDateTime(StartDate) , @"endD", Convert.ToDateTime(EndDate));
        }
    }

    public void AddCandidate(string VoterID, int PositionID, int ElectionID)
    {
        table = new DataTable();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("addCandidate", @"Voter_ID", VoterID, @"Position_ID", PositionID, @"Election_ID", ElectionID);
        }
    }

    public void AddPosition(string Position, int Max)
    {
        table = new DataTable();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("addPosition", @"PosName", Position, @"max_vote", Max);
        }
    }

    public void AddVote(int CandidateID)
    {
        table = new DataTable();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("addVote", @"CandidateID", CandidateID);
        }
    }

    public void AddVoter(string VoterID, string FN, string LN, string MN)
    {
        table = new DataTable();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("addVoter", @"Voter_ID", VoterID, @"First_Name", FN, @"Last_Name", LN, @"Middle_Name", MN);
        }
    }

    public void AddWinners(int ElecID)
    {
        table = new DataTable();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("addWinners", @"ElecID", ElecID);
        }
    }

    public void hasVoted(int VoterID)
    {
        table = new DataTable();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("hasVoted", @"VoterID", VoterID, @"HasAlreadyVoted", 1);
        }
    }

    public List<string> GetElectionDate()
    {
        table = new DataTable();
        List<string> cond = new List<string>();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("getElectionDate");
            foreach (DataRow dt in table.Rows)
            {
                cond.Add(dt.Field<string>(0));
            }
        }
        return cond;
    }

    public List<string> GetStartDate()
    {
        table = new DataTable();
        List<string> cond = new List<string>();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("getStartDate");
            foreach (DataRow dt in table.Rows)
            {
                cond.Add(dt.Field<string>(0));
            }
        }
        return cond;
    }

    public List<string> GetEndDate()
    {
        table = new DataTable();
        List<string> cond = new List<string>();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("getEndDate");
            foreach (DataRow dt in table.Rows)
            {
                cond.Add(dt.Field<string>(0));
            }
        }
        return cond;
    }

    public List<string> GetPostions()
    {
        table = new DataTable();
        List<string> cond = new List<string>();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("getPositions");
            foreach (DataRow dt in table.Rows)
            {
                cond.Add(dt.Field<string>(0));
            }
        }
        return cond;
    }

    public List<string> GetVoter()
    {
        table = new DataTable();
        List<string> cond = new List<string>();
        Helper.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        using (Helper db = new Helper())
        {
            table = db.ExecDataTableProc("getVoter");
            foreach (DataRow dt in table.Rows)
            {
                cond.Add(dt.Field<string>(0));
            }
        }
        return cond;
    }
}


